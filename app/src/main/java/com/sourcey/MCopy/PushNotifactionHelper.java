package com.sourcey.MCopy;

/**
 * Created by bhavik on 08-10-2017.
 */

import android.telephony.SmsManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;

public class PushNotifactionHelper {
    /*public final static String AUTH_KEY_FCM = "key=AAAA8j7v86E:APA91bEiTEAqAf4awzsBn9nDbSWmmnIgpV0oWJNC00Na6RHk9WUnZzFmiKTpcPG3Y5_BZL1YS8Rzxeqyz12ey5jKylrMF-wJvQm1ra1hs-4ilt_JVpOLBNfeFW5ojt8LgJRP8RY-0EIU";
    public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public static void sendPushNotification(List<String> deviceTokenList) {
        Sender sender = new Sender(AUTH_KEY_FCM);
        Message msg = new Message.Builder().addData("message", "Message body")
                .build();
        try {
            MulticastResult result = sender.send(msg, deviceTokenList, 5);
            for (Result r : result.getResults()) {
                if (r.getMessageId() != null)
                    System.out.println("Push Notification Sent Successfully");
                else
                    System.out.println("ErrorCode " + r.getErrorCodeName());
            }
        } catch (IOException e) {
            System.out.println("Error " + e.getLocalizedMessage());
        }
    }
}*/
    public static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    public static final String FCM_SERVER_API_KEY    = "key=AIzaSyDHEPHV1F6UMuzdiEK3PYT5PJmAMXfQbdE";
    //private static final String deviceRegistrationId = FirebaseInstanceId.getInstance().getToken();

    public static void sendPushNotification(String deviceRegistrationId)
    {
        int responseCode = -1;
        String responseBody = null;
        try
        {
            System.out.println("Sending FCM request");
            byte[] postData = getPostData(deviceRegistrationId);

            URL url = new URL(FCM_URL);
            HttpsURLConnection httpURLConnection = (HttpsURLConnection)url.openConnection();

            //set timeputs to 10 seconds
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(10000);

            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postData.length));
            httpURLConnection.setRequestProperty("Authorization", "key="+FCM_SERVER_API_KEY);

            OutputStream out = httpURLConnection.getOutputStream();
            out.write(postData);
            out.close();
            responseCode = httpURLConnection.getResponseCode();
            //success
            if (responseCode == httpURLConnection.HTTP_OK)
            {
                responseBody = convertStreamToString(httpURLConnection.getInputStream());
                System.out.println("FCM message sent : " + responseBody);
            }
            //failure
            else
            {
                responseBody = convertStreamToString(httpURLConnection.getErrorStream());
                System.out.println("Sending FCM request failed for regId: " + deviceRegistrationId + " response: " + responseBody);
            }
        }
        catch (IOException ioe)
        {
            System.out.println("IO Exception in sending FCM request. regId: " + deviceRegistrationId);
            ioe.printStackTrace();
        }
        catch (Exception e)
        {
            System.out.println("Unknown exception in sending FCM request. regId: " + deviceRegistrationId);
            e.printStackTrace();
        }
    }

    public static byte[] getPostData(String registrationId) throws JSONException {
        HashMap<String, String> dataMap = new HashMap<>();
        JSONObject payloadObject = new JSONObject();

        dataMap.put("name", "Aniket!");
        dataMap.put("country", "India");

        JSONObject data = new JSONObject(dataMap);;
        payloadObject.put("data", data);
        payloadObject.put("to", registrationId);

        return payloadObject.toString().getBytes();
    }

    public static String convertStreamToString (InputStream inStream) throws Exception
    {
        InputStreamReader inputStream = new InputStreamReader(inStream);
        BufferedReader bReader = new BufferedReader(inputStream);

        StringBuilder sb = new StringBuilder();
        String line = null;
        while((line = bReader.readLine()) != null)
        {
            sb.append(line);
        }

        return sb.toString();
    }
}