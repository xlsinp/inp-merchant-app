package com.sourcey.MCopy.modal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bhavik on 03-05-2017.
 */

public class LoginModel {

    @SerializedName("name")
    private String name;
    @SerializedName("lastname")
    private String  lastname;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private String  mobile;
    @SerializedName("qrID")
    private String arID;
    @SerializedName("rewards")
    private String  rewards;
    @SerializedName("dob")
    private String dob;
    @SerializedName("password")
    private String  password;


    @SerializedName("qrCode")
    private String  qrCode;

    public LoginModel(String name, String lastname, String email, String mobile, String arID, String rewards, String dob, String password, String qrCode) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.mobile = mobile;
        this.arID = arID;
        this.rewards = rewards;
        this.dob = dob;
        this.password = password;
        this.qrCode = qrCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getArID() {
        return arID;
    }

    public void setArID(String arID) {
        this.arID = arID;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

}