package com.sourcey.MCopy.modal;

/**
 * Created by bhavik on 07-10-2017.
 */

public class FCMModel {
    String name,lastname,email,mobile,qrID,rewards,dob,password,qrCode;

    public FCMModel(String name, String lastname, String email, String mobile, String qrID, String rewards, String dob, String password, String qrCode) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.mobile = mobile;
        this.qrID = qrID;
        this.rewards = rewards;
        this.dob = dob;
        this.password = password;
        this.qrCode = qrCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getQrID() {
        return qrID;
    }

    public void setQrID(String qrID) {
        this.qrID = qrID;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}