package com.sourcey.MCopy.modal;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class UserDetails{

	@SerializedName("password")
	private Object password;

	@SerializedName("qrCode")
	private String qrCode;

	@SerializedName("dob")
	private String dob;

	@SerializedName("name")
	private String name;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("qrID")
	private String qrID;

	@SerializedName("email")
	private String email;

	@SerializedName("rewards")
	private int rewards;

	@SerializedName("lastname")
	private Object lastname;

	public void setPassword(Object password){
		this.password = password;
	}

	public Object getPassword(){
		return password;
	}

	public void setQrCode(String qrCode){
		this.qrCode = qrCode;
	}

	public String getQrCode(){
		return qrCode;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setQrID(String qrID){
		this.qrID = qrID;
	}

	public String getQrID(){
		return qrID;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setRewards(int rewards){
		this.rewards = rewards;
	}

	public int getRewards(){
		return rewards;
	}

	public void setLastname(Object lastname){
		this.lastname = lastname;
	}

	public Object getLastname(){
		return lastname;
	}

	@Override
 	public String toString(){
		return 
			"UserDetails{" + 
			"password = '" + password + '\'' + 
			",qrCode = '" + qrCode + '\'' + 
			",dob = '" + dob + '\'' + 
			",name = '" + name + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",qrID = '" + qrID + '\'' + 
			",email = '" + email + '\'' + 
			",rewards = '" + rewards + '\'' + 
			",lastname = '" + lastname + '\'' + 
			"}";
		}
}