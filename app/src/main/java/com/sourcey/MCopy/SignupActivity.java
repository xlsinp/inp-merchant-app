package com.sourcey.MCopy;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.google.firebase.FirebaseApp;
import com.sourcey.MCopy.fragments.InviteCode;
import com.sourcey.MCopy.fragments.NameFragment;

import butterknife.ButterKnife;
import butterknife.Bind;

public class SignupActivity extends AppCompatActivity {

    @Bind(R.id.signupframe) FrameLayout _signupframe;

    ViewPager viewPager;
    SliderPageAdapter sliderPageAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
//        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);
        //loadFragment(new InviteCode());
        viewPager = (ViewPager) findViewById(R.id.signupframe);
        sliderPageAdapter = new SliderPageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sliderPageAdapter);
    }

    /*public void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.signupframe, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }*/

    public class SliderPageAdapter extends FragmentPagerAdapter{

        public SliderPageAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return new InviteCode();
        }

        @Override
        public int getCount() {
            return 0;
        }
    }
}