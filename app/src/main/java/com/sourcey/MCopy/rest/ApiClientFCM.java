package com.sourcey.MCopy.rest;


import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by bhavik on 30-04-2017.
 */

public class ApiClientFCM {
    //Base Url Common URL
    //public static final String BASE_URL = "http://xlsoftek.us-east-1.elasticbeanstalk.com";
    public static final String BASE_URL = "http://cvvnm.us-east-1.elasticbeanstalk.com";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    //.addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
