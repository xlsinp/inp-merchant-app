package com.sourcey.MCopy.rest;

import com.sourcey.MCopy.modal.FCMModel;
import com.sourcey.MCopy.modal.LoginModel;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by bhavik on 30-04-2017.
 */

public interface ApiInterfaceFCM {

    @FormUrlEncoded
    @POST("/inp/send/")
    public Call<FCMModel> getSignupResponse(
            @Field("mobn") String mobn,
            @Field("str") String str);
}