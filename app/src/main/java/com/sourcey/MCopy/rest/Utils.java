package com.sourcey.MCopy.rest;

import android.content.Context;
import android.content.DialogInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Response;


/**
 * Created by bhavik on 30-04-2017.
 */

public class Utils {
    public static void info_alert(Context context, String title, String message) {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setPositiveButton("ÖK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.show();
    }

    public static String dobuffer(Response result, String output) {
        BufferedReader reader = null;
        //An string to store output from the server
        try {
            //Initializing buffered reader
            reader = new BufferedReader(new InputStreamReader(result.raw().body().byteStream()));
            //Reading the output in the string
            output = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output;
    }
}