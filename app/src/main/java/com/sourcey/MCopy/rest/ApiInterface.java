package com.sourcey.MCopy.rest;

import com.sourcey.MCopy.modal.LoginModel;
import com.sourcey.MCopy.modal.UserDetails;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by bhavik on 30-04-2017.
 */

public interface ApiInterface {
    /*@FormUrlEncoded
    @POST("inp/sureshcvv21@gmail.com/1234")
    public Call<pincodecheck> getPincodeStatus(
            @Field("pincode") String pincode);*/

 /*   @GET("inp/sureshcvv21@gmail.com/1234")
    //public String getCategory();
    Call<ResponseBody> getLoginResponse();
*/

    @GET("/inp/mar/{user}/{pass}")
        //public String getCategory();
    Call<LoginModel> getLoginResponse(@Path("user") String user, @Path("pass") String pass);


    //fname=Bhavik&mobn=9825516928&email=bhavikbagdai@gmail.com&pwd=1245
    /*@GET("inp/create?fname={fname}&mobn={mobn}&email={email}&pwd={pwd}")
        //public String getCategory();
    Call<ResponseBody> getSignupResponse(@Path("fname") String fname, @Path("mobn") String mobn,@Path("email") String email, @Path("pwd") String pwd);
*/

    @FormUrlEncoded
    @POST("/inp/mar_create/")
    public Call<ResponseBody> getSignupResponse(
            @Field("fname") String fname,
            @Field("email") String email,
            @Field("mobn") String mobn,
            @Field("pwd") String pwd);

    @FormUrlEncoded
    @POST("/inp/update")
    public Call<ResponseBody> DecreasePoint(
            @Field("mobn") String mobn,
            @Field("str") String str,
            @Field("rewards") String rewards);

    @GET("/send/{mobile}/{msg}")
        //public String getCategory();
    Call<ResponseBody> getFCMResponse(@Path("mobile") String mobile, @Path("msg") String msg);

    @GET("/inp/UpdateRe/{mobile}")
        //public String getCategory();
    Call<ResponseBody> getUpdatedPoints(@Path("mobile") String mobile);

    @GET("/inp/{mobile}")
        //public String getCategory();
    Call<UserDetails> getUpdatedPointsM(@Path("mobile") String mobile);
}