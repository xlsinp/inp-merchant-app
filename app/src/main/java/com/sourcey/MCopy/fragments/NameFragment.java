package com.sourcey.MCopy.fragments;

import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;


import com.sourcey.MCopy.R;
import com.sourcey.MCopy.SignupActivity;

import static android.content.Context.MODE_PRIVATE;

public class NameFragment extends Fragment {
    View view;
    Button firstButton;
    AppCompatEditText etFname;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_name, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        etFname = (AppCompatEditText)view.findViewById(R.id.etfname);
        firstButton = (Button) view.findViewById(R.id.btnnext);
        // perform setOnClickListener on first Button

        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        //etFname.setText(pref.getString("FirstName",""));

        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("FirstName",etFname.getText().toString());
                editor.commit();
                //((SignupActivity)getActivity()).loadFragment(new MailFragment());
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.signupframe, new MailFragment());
                transaction.commit();
            }
        });
        return view;
    }
}