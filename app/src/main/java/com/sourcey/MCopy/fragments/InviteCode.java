package com.sourcey.MCopy.fragments;

import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.MCopy.R;
import com.sourcey.MCopy.SignupActivity;

import org.w3c.dom.Text;

import static android.content.Context.MODE_PRIVATE;

public class InviteCode extends Fragment {
    View view;
    Button firstButton;
    AppCompatEditText etFname;
    TextView txtname;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_name, container, false);
        txtname = (TextView)view.findViewById(R.id.txtname);
        etFname = (AppCompatEditText)view.findViewById(R.id.etfname);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        firstButton = (Button) view.findViewById(R.id.btnnext);
        // perform setOnClickListener on first Button
        txtname.setText("Invite Code");
        etFname.setHint("Invite Code");
        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etFname.getText().toString().equals("INPMAR2017")) {
                    editor.putString("InviteCode", etFname.getText().toString());
                    editor.commit();
                    //((SignupActivity) getActivity()).loadFragment(new BusinessName());
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.signupframe, new BusinessName());
                    transaction.commit();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Please Contact Administrator for Invite Code", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }
}