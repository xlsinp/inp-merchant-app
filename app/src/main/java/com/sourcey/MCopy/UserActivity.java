package com.sourcey.MCopy;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.sourcey.MCopy.modal.FCMModel;
import com.sourcey.MCopy.modal.UserDetails;
import com.sourcey.MCopy.rest.ApiClient;
import com.sourcey.MCopy.rest.ApiClientFCM;
import com.sourcey.MCopy.rest.ApiInterface;
import com.sourcey.MCopy.rest.ApiInterfaceFCM;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by srikiranvadla on 8/4/2017.
 */

public class UserActivity extends AppCompatActivity {
    private static final String TAG = "UserActivity";
    String rvalues;
    TextView QrCodeval;
    EditText etredimpoints;
    //String Spointsval,Snameval,Semailval,Saltid,Spointpws,SMobile;
    AppCompatButton btnScan,btnScanoffer,btn_add,btn_redeem;

    TextView pointsval,nameval,emailval,altid;
    String Spointsval,Snameval,Semailval,Saltid,Spointpws,SMobile,SqrCode;
    // StringBuffer Spointsval,Snameval,Semailval,Saltid,Spointpws,SMobile,SqrCode;
    ImageView qrcode;
    private int WIDTH = 200;
    Bitmap bitmap;
    int fromid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        fromid = getIntent().getExtras().getInt("id");
        rvalues = getIntent().getExtras().getString("values");

        init();
        directRedeem(rvalues);
        //fetchpoints(rvalues);

        try {
            bitmap = TextToImageEncode(rvalues);
            qrcode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        if(fromid == 1) {
            String[] values = rvalues.split(",");
            btn_add.setVisibility(View.GONE);
            etredimpoints.setVisibility(View.GONE);
            //btn_redeem.setVisibility(View.VISIBLE);
            pointsval.setText(values[1]);
            //nameval.setText(values[2]);
            try {
                emailval.setText(values[4]);
            } catch (Exception e) {
                emailval.setText("");
            }
            altid.setText(values[2]);

            try {
                int num = Integer.parseInt(values[0]);
                if(num==1) {
                    bitmap = TextToImageEncode(rvalues,Color.RED);
                } else if(num==2) {
                    bitmap = TextToImageEncode(rvalues,Color.GRAY);
                } else if(num==3) {
                    bitmap = TextToImageEncode(rvalues,Color.BLUE);
                } else if(num==4) {
                    bitmap = TextToImageEncode(rvalues,Color.GREEN);
                }
                qrcode.setImageBitmap(bitmap);
            } catch (WriterException e) {
                e.printStackTrace();
            }

        } else {
            btn_add.setVisibility(View.VISIBLE);
            btn_redeem.setVisibility(View.GONE);

            String[] values = rvalues.split(",");
            pointsval.setText(values[1]);
            nameval.setText(values[2]);
            try {
                emailval.setText(values[4]);
            } catch (Exception e) {
            }
            altid.setText(values[3]);
        }
    }

    public void fetchpoints(String rvalues)
    {
        String [] values = rvalues.split(",");

        //Toast.makeText(this, "" + values[1], Toast.LENGTH_SHORT).show();
        pointsval.setText(values[1]);
        //nameval.setText(values[2]);
        try{
            emailval.setText(values[4]);
        } catch (Exception e){}
        altid.setText(values[2]);

        //Following api call will work to fetch the updated points from server based on mobile number
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> result1 = apiService.getUpdatedPoints(values[3]);
        result1.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                try {
                    //String g1 = new Gson().toJson(response);
                    //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                    if (response.isSuccess()) {
                        pointsval.setText("" + response.body().string());

                    } else {
                        Toast.makeText(UserActivity.this, "No Points Found for this users", Toast.LENGTH_SHORT).show();
                        pointsval.setText("0");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }
    private void init() {
        //QrCodeval = (TextView) findViewById(R.id.QrCodeval);

        pointsval = (TextView) findViewById(R.id.pointsval);
        nameval = (TextView) findViewById(R.id.nameval);
        emailval = (TextView) findViewById(R.id.emailval);
        altid = (TextView) findViewById(R.id.altid);

        qrcode  = (ImageView) findViewById(R.id.qrcode);
        if(rvalues!="") {
            String[] values = rvalues.split(",");
            if(values.length==6)
                fetchvaluesonMobilenumber(values[3]);
            else
                fetchvaluesonMobilenumber(values[2]);
        }
        btnScan = (AppCompatButton) findViewById(R.id.btn_scan);
        etredimpoints = (EditText)findViewById(R.id.etredimpoints);

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            rvalues="";
            IntentIntegrator integrator = new IntentIntegrator(UserActivity.this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.setPrompt("Scan");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();

               // QRCodeReader barcode_scanner = (QRCodeReader)findViewById(R.id.barcode_scanner);
            }
        });

        btn_redeem = (AppCompatButton) findViewById(R.id.btn_redeem);
        btn_redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                String [] values = rvalues.split(",");
                int points = 0;
                try {
                    points = Integer.parseInt(etredimpoints.getText().toString());
                }catch (Exception e){
                    points = 0;
                }
                if(rvalues=="" || points==0)
                {
                    Toast.makeText(UserActivity.this, " Either Code Not Scanned or Points to reedem not Provide." , Toast.LENGTH_SHORT).show();
                }else {
                    Call<ResponseBody> result = apiService.DecreasePoint(values[3], "dec", etredimpoints.getText().toString());
                    result.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                            try {
                                String resp = response.body().string();
                                if (resp.length() > 0) {
                                    Toast.makeText(UserActivity.this, "Points Redeemed" + etredimpoints.getText().toString(), Toast.LENGTH_SHORT).show();
                                    //Call<FCMModel> result = apiService.DecreasePoint(values[3], "dec", etredimpoints.getText().toString());
                                    String [] values = rvalues.split(",");
                                    Call<ResponseBody> result = apiService.getFCMResponse(values[3],"-"+etredimpoints.getText().toString());
                                    result.enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                                            try {
                                                //String g1 = new Gson().toJson(response);
                                                //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                                                if(response.isSuccess()) {
                                                    int orPoints = Integer.parseInt(pointsval.getText().toString());
                                                    int redpoint = Integer.parseInt(etredimpoints.getText().toString());
                                                    int rempoint = orPoints - redpoint;
                                                    pointsval.setText(""+rempoint);
                                                    //Toast.makeText(UserActivity.this, "Message Delivered", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    //Toast.makeText(UserActivity.this, "Message Not Delivered", Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (Exception e){
                                                e.printStackTrace();
                                                //Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                            finally {
                                                etredimpoints.setText("");
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {

                                        }
                                    });
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            t.printStackTrace();
                        }
                    });
                    // QRCodeReader barcode_scanner = (QRCodeReader)findViewById(R.id.barcode_scanner);
                }
            }
        });

        btn_add = (AppCompatButton) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                String [] values = rvalues.split(",");
                int points = 0;
                try {
                    points = Integer.parseInt(etredimpoints.getText().toString());
                }catch (Exception e){
                    points = 0;
                }
                if(rvalues=="" || points==0)
                {
                    Toast.makeText(UserActivity.this, " Either Code Not Scanned or Points to reedem not Provide." , Toast.LENGTH_SHORT).show();
                }else {
                    Call<ResponseBody> result = apiService.DecreasePoint(values[3], "inr", etredimpoints.getText().toString());
                    result.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                            try {
                                String resp = response.body().string();
                                if (resp.length() > 0) {
                                    Toast.makeText(UserActivity.this, "Points Increased" + etredimpoints.getText().toString(), Toast.LENGTH_SHORT).show();
                                    //Call<FCMModel> result = apiService.DecreasePoint(values[3], "dec", etredimpoints.getText().toString());
                                    String [] values = rvalues.split(",");
                                    Call<ResponseBody> result = apiService.getFCMResponse(values[3],etredimpoints.getText().toString());
                                    result.enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                                            try {
                                                //String g1 = new Gson().toJson(response);
                                                //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                                                if(response.isSuccess()) {
                                                    int orPoints = Integer.parseInt(pointsval.getText().toString());
                                                    int redpoint = Integer.parseInt(etredimpoints.getText().toString());
                                                    int rempoint = orPoints + redpoint;
                                                    pointsval.setText(""+rempoint);
                                                   // Toast.makeText(UserActivity.this, "Message Delivered", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    //Toast.makeText(UserActivity.this, "Message Not Delivered", Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (Exception e){
                                                e.printStackTrace();
                                                //Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                            finally {
                                                etredimpoints.setText("");
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {

                                        }
                                    });

                                    //rvalues = "";
                                    //etredimpoints.setText("");
                                    /*Intent intent = new Intent();
                                    intent.putExtra("RemPoints", String.valueOf(RemPoints));
                                    setResult(RESULT_OK, intent);
                                    finish();*/
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            t.printStackTrace();
                        }
                    });
                    // QRCodeReader barcode_scanner = (QRCodeReader)findViewById(R.id.barcode_scanner);
                }
            }
        });
    }

    private void fetchvaluesonMobilenumber(String value) {
        //Following api call will work to fetch the updated points from server based on mobile number
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<UserDetails> result1 = apiService.getUpdatedPointsM(value);
        result1.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Response<UserDetails> response, Retrofit retrofit) {
                try {
                    //String g1 = new Gson().toJson(response);
                    //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                    if (response.isSuccess()) {
                        pointsval.setText("" + response.body().getRewards());
                        nameval.setText("" + response.body().getName());
                        emailval.setText("" + response.body().getEmail());
                        altid.setText("" + response.body().getMobile());

                    } else {
                        Toast.makeText(UserActivity.this, "No Points Found for this users", Toast.LENGTH_SHORT).show();
                        pointsval.setText("0");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    public void directRedeem(final String rvalues){
        Log.d("MainActivity", "Scanned");
        //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();

       //Toast.makeText(this, "" + rvalues, Toast.LENGTH_SHORT).show();
        try {
            bitmap = TextToImageEncode(rvalues);
            qrcode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        String[] values = rvalues.split(",");

        if (values.length == 6) {
            pointsval.setText(values[1]);
            nameval.setText(values[2]);
            emailval.setText(values[4]);
            altid.setText(values[5]);

            btn_add.setVisibility(View.VISIBLE);
            btn_redeem.setVisibility(View.GONE);

            //Following api call will work to fetch the updated points from server based on mobile number
            final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseBody> result1 = apiService.getUpdatedPoints(values[3]);
            result1.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                    try {
                        //String g1 = new Gson().toJson(response);
                        //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                        if (response.isSuccess()) {
                            pointsval.setText("" + response.body().string());
                        } else {
                            Toast.makeText(UserActivity.this, "No Points Found for this users", Toast.LENGTH_SHORT).show();
                            pointsval.setText("0");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
        if(values.length == 4){

            //Todo : Bhavik This is Offer page Scan area.
            pointsval.setText(values[1]);
            altid.setText(values[2]);
            nameval.setText("");
            emailval.setText("");

            btn_add.setVisibility(View.GONE);
            etredimpoints.setVisibility(View.GONE);
            //btn_redeem.setVisibility(View.VISIBLE);

            final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseBody> resultoffer = apiService.DecreasePoint(values[2], "dec", values[1]);
            resultoffer.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                    try {
                        String resp = response.body().string();
                        if (resp.length() > 0) {
                            Toast.makeText(UserActivity.this, "Points Redeemed" + etredimpoints.getText().toString(), Toast.LENGTH_SHORT).show();
                            //Call<FCMModel> result = apiService.DecreasePoint(values[3], "dec", etredimpoints.getText().toString());
                            String [] values = rvalues.split(",");
                           // Toast.makeText(UserActivity.this, "" + values[2], Toast.LENGTH_LONG).show();
                            Call<ResponseBody> result = apiService.getFCMResponse(values[2],"-"+values[1]);
                            result.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                                    try {
                                        //String g1 = new Gson().toJson(response);
                                        //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                                        if(response.isSuccess()) {
                                            int orPoints = Integer.parseInt(pointsval.getText().toString());
                                            int redpoint = Integer.parseInt(etredimpoints.getText().toString());
                                            int rempoint = orPoints - redpoint;
                                            pointsval.setText(""+rempoint);
                                            //Toast.makeText(UserActivity.this, "Message Delivered", Toast.LENGTH_SHORT).show();
                                        } else {
                                            //Toast.makeText(UserActivity.this, "Message Not Delivered", Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception e){
                                        e.printStackTrace();
                                        //Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                    finally {
                                        etredimpoints.setText("");
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {

                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned");
                    //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                    pointsval.setText(result.getContents());
                    rvalues = pointsval.getText().toString();
                    //Toast.makeText(this, "" + rvalues, Toast.LENGTH_SHORT).show();
                    try {
                        bitmap = TextToImageEncode(rvalues);
                        qrcode.setImageBitmap(bitmap);
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }

                    String[] values = pointsval.getText().toString().split(",");

                    if (values.length == 6) {
                        pointsval.setText(values[1]);
                        nameval.setText(values[2]);
                        emailval.setText(values[4]);
                        altid.setText(values[5]);

                        btn_add.setVisibility(View.VISIBLE);
                        btn_redeem.setVisibility(View.GONE);

                        //Following api call will work to fetch the updated points from server based on mobile number
                        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        Call<ResponseBody> result1 = apiService.getUpdatedPoints(values[3]);
                        result1.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                                try {
                                    //String g1 = new Gson().toJson(response);
                                    //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                                    if (response.isSuccess()) {
                                        pointsval.setText("" + response.body().string());
                                    } else {
                                        Toast.makeText(UserActivity.this, "No Points Found for this users", Toast.LENGTH_SHORT).show();
                                        pointsval.setText("0");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                  //  Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {

                            }
                        });
                    }
                    if(values.length == 4){

                        //Todo : Bhavik This is Offer page Scan area.
                        pointsval.setText(values[1]);
                        altid.setText(values[2]);
                        nameval.setText("");
                        emailval.setText("");

                        btn_add.setVisibility(View.GONE);
                        etredimpoints.setVisibility(View.GONE);
                       // btn_redeem.setVisibility(View.VISIBLE);

                        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        Call<ResponseBody> resultoffer = apiService.DecreasePoint(values[2], "dec", values[1]);
                        resultoffer.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                                try {
                                    String resp = response.body().string();
                                    if (resp.length() > 0) {
                                        Toast.makeText(UserActivity.this, "Points Redeemed" + etredimpoints.getText().toString(), Toast.LENGTH_SHORT).show();
                                        //Call<FCMModel> result = apiService.DecreasePoint(values[3], "dec", etredimpoints.getText().toString());
                                        String [] values = rvalues.split(",");
                                        Call<ResponseBody> result = apiService.getFCMResponse(values[2],"-"+values[1]);
                                        result.enqueue(new Callback<ResponseBody>() {
                                            @Override
                                            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                                                try {
                                                    //String g1 = new Gson().toJson(response);
                                                    //Toast.makeText(UserActivity.this, "" + response.message().toString(), Toast.LENGTH_SHORT).show();
                                                    if(response.isSuccess()) {
                                                        int orPoints = Integer.parseInt(pointsval.getText().toString());
                                                        int redpoint = Integer.parseInt(etredimpoints.getText().toString());
                                                        int rempoint = orPoints - redpoint;
                                                        pointsval.setText(""+rempoint);
                                                       // Toast.makeText(UserActivity.this, "Message Delivered", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        //Toast.makeText(UserActivity.this, "Message Not Delivered", Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (Exception e){
                                                    e.printStackTrace();
                                                    //Toast.makeText(UserActivity.this, "" + e.toString(), Toast.LENGTH_SHORT).show();
                                                }
                                                finally {
                                                    etredimpoints.setText("");
                                                }
                                            }

                                            @Override
                                            public void onFailure(Throwable t) {

                                            }
                                        });
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                t.printStackTrace();
                            }
                        });

                }
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    500, 500, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ? Color.BLACK:Color.WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    Bitmap TextToImageEncode(String Value,int color) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    500, 500, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ? color:Color.WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }
}