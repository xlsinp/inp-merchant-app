package com.sourcey.MCopy;

import android.Manifest;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sourcey.MCopy.modal.LoginModel;
import com.sourcey.MCopy.rest.ApiClient;
import com.sourcey.MCopy.rest.ApiInterface;

import io.fabric.sdk.android.Fabric;

import butterknife.ButterKnife;
import butterknife.Bind;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginActivity extends RuntimePermissionsActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    private static final int REQUEST_PERMISSIONS = 20;
    SharedPreferences pref;

    @Bind(R.id.input_email) EditText _emailText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.btn_login) Button _loginButton;
    @Bind(R.id.link_signup) TextView _signupLink;

    // @Bind(R.id.link_signup) TextView _loginLink;
    //int attempt_counter=5;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Fabric.with(this, new Crashlytics());


        pref = getSharedPreferences("MyPref", MODE_PRIVATE);

        LoginActivity.super.requestAppPermissions(new String[]{Manifest.permission.INTERNET}, R.string.runtime_permissions_txt
                , REQUEST_PERMISSIONS);
        LoginButton();
    }

    @Override
    public void onPermissionsGranted(final int requestCode) {
        //Toast.makeText(this, "Permissions Received.", Toast.LENGTH_LONG).show();
    }


    public void LoginButton(){
        _emailText = (EditText)findViewById(R.id.input_email);
        _passwordText = (EditText)findViewById(R.id.input_password);
        _loginButton = (Button)findViewById(R.id.btn_login);

        String email = pref.getString("email","");
        String password = pref.getString("password","");

        if(!email.equals("") || !password.equals(""))
        {
            _emailText.setText(email);
            _passwordText.setText(password);
            //_loginButton.performClick();
            login();
        }

        /*_loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //login();
                ///1234
                if (_emailText.getText().toString().equals("sureshcvv21@gmail.com")&& _passwordText.getText().toString().equals("1234"))
                {
                    Toast.makeText(LoginActivity.this, "Email and Password is correct", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this,UserActivity.class);
                    startActivity(intent);
                }

                *//*else{
                    Toast.makeText(LoginActivity.this, "Email and Password is NOT correct", Toast.LENGTH_SHORT).show();
                }*//*
               // Intent intent = new Intent(getApplicationContext(), UserActivity.class);
               // startActivity(intent);
            }
        });
*/
        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                login();
                /*Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);*/
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(true);
        final SharedPreferences.Editor editor = pref.edit();
        editor.putString("email",_emailText.getText().toString());
        editor.putString("password",_passwordText.getText().toString());
        editor.commit();
        /*final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
          R.style.AppTheme_Dark_Dialog);
       progressDialog.setIndeterminate(true);
       progressDialog.setMessage("Authenticating...");
       progressDialog.show();*/

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        // TODO: Implement your own authentication logic here.
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<LoginModel> result = apiService.getLoginResponse(email,password);
        result.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Response<LoginModel> response, Retrofit retrofit) {

                String email = response.body().getEmail();
                if(email != null){
                    Intent intent = new Intent(getApplicationContext(), UserActivityPrim.class);
                    intent.putExtra("name",response.body().getName());
                    intent.putExtra("email",response.body().getEmail());
                    intent.putExtra("altid",response.body().getArID());
                    intent.putExtra("point",response.body().getRewards());
                    intent.putExtra("pws",_emailText.getText().toString());
                    intent.putExtra("mobile",response.body().getMobile());

                    /*SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("name",response.body().getName());
                    editor.putString("email",response.body().getEmail());
                    editor.putString("altid",response.body().getArID());
                    editor.putString("point",response.body().getRewards());
                    editor.putString("mobile",response.body().getMobile());
                    editor.putString("pws",_passwordText.getText().toString());
                    editor.commit();*/

                    startActivity(intent);
                    //startActivityForResult(intent, REQUEST_SIGNUP);
                    //finish();
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                }
                else{
                    Toast.makeText(LoginActivity.this, "Wrong UserName or Password", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        //progressDialog.dismiss();
                    }
                }, 3000);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                //this.finish();
            }
        }
    }

    /*@Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }*/

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        //finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }


    //public void onClick(View )

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}
