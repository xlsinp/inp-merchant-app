package com.sourcey.MCopy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.sourcey.MCopy.rest.ApiClient;
import com.sourcey.MCopy.rest.ApiInterface;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by srikiranvadla on 8/4/2017.
 */

public class UserActivityPrim extends AppCompatActivity {
    private static final String TAG = "UserActivity";
    String rvalues;
    AppCompatButton btnScan,btn_logout;

    ImageView qrcode;
    AppCompatButton btnredeem;
    private int WIDTH = 200;
    Bitmap bitmap;
    private SharedPreferences pref;
    private AppCompatButton btnScanOffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_prim);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        init();
    }

    private void init() {
        //QrCodeval = (TextView) findViewById(R.id.QrCodeval);

        btnScan = (AppCompatButton) findViewById(R.id.btn_scan);

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(UserActivityPrim.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();

               // QRCodeReader barcode_scanner = (QRCodeReader)findViewById(R.id.barcode_scanner);

            }
        });

        btnScanOffer = (AppCompatButton) findViewById(R.id.btn_scanoffer);

        btnScanOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(UserActivityPrim.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();

                // QRCodeReader barcode_scanner = (QRCodeReader)findViewById(R.id.barcode_scanner);

            }
        });

        btn_logout = (AppCompatButton) findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pref = getSharedPreferences("MyPref", MODE_PRIVATE);
                final SharedPreferences.Editor editor = pref.edit();
                editor.putString("email","");
                editor.putString("password","");
                editor.commit();

                Intent inte = new Intent(UserActivityPrim.this,LoginActivity.class);
                startActivity(inte);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned");
                //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                rvalues = result.getContents();
                String lencount[] = rvalues.split(",");
                if(lencount.length==4) {
                    Intent inte = new Intent(this, UserActivity.class);
                    inte.putExtra("id",1);
                    inte.putExtra("values", rvalues);
                    startActivity(inte);
                } else {
                    Intent inte = new Intent(this, UserActivity.class);
                    inte.putExtra("id",2);
                    inte.putExtra("values", rvalues);
                    startActivity(inte);
                }
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}